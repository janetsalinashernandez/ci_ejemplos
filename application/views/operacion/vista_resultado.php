<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Operación</title>

	<style type="text/css">

	::selection{ background-color: #E13300; color: white; }
	::moz-selection{ background-color: #E13300; color: white; }
	::webkit-selection{ background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body{
    position:relative;
    height:250px;
	}

  #menu{
    position:absolute;
    top:0;
    left:0;
		margin: 14px;
    width: 20%;
  }
	
  #resultado{
    position:absolute;
    top:0;
    right:0;
    text-align: left;
		margin: 14px;
    width: 75%;
  }

	p.footer{
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}
	
	#container{
		margin: 10px;
		border: 1px solid #D0D0D0;
		-webkit-box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div id="container">

  <h1>Controlador Operación</h1>
	<div id="body">
    <div id="menu">
		<h1>Menú</h1>
    <ul>
      <li><a href="/index.php/operacion">Operación</a></li>
        <ul>
          <li><a href="/index.php/operacion/suma">Suma</a></li>
          <li><a href="/index.php/operacion/resta">Resta</a></li>
          <li><a href="/index.php/operacion/multiplicacion">Multiplicación</a></li>
          <li><a href="/index.php/operacion/division">División</a></li>
        </ul>
      <li><a href="/index.php">Inicio</a></li>
    </ul>
    </div>

    <div id="resultado">
    <h1><?php echo $titulo_principal; ?></h1>
        <p><?php echo $mensaje; ?></p>
    </div>

	</div>


  <p class="footer">
    <strong>Controlador:</strong> application/controllers/<strong>operacion.php</strong></br >
    <strong>Vista:</strong> application/views/operacion/<strong>vista_resultado.php</strong>
  </p>

</div>

</body>
</html>

