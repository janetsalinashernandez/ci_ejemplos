<div class="info">
<div class="titulo">ESENCIA</div>
<div class="infoconte">
<div class="imagene"><img src="/imagenes/imgesencia.jpg" width="245" height="360" border="0"></div>
<div class="textos">
Origen Oaxaca es un espacio diseñado y orientado al exquisito goce del paladar. En este lugar se fusionan conceptos gastronómicos como, comedor, taller y compras.<br/>
<br/>
La belleza arquitectónica del lugar es uno de los ingredientes principales en nuestras experiencias Origen. <br/>
<br/>
Frescura e innovación son sellos característicos de nuestros servicios. En Origen Oaxaca estamos listos para servir momentos memorables.<br/>

</div>
</div>
</div>
