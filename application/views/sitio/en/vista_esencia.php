<div class="info">
<div class="titulo">ESSENCE</div>
<div class="infoconte">
<div class="imagene"><img src="/imagenes/imgesencia.jpg" width="245" height="360" border="0"></div>
<div class="textos">
Origen Oaxaca is a space designed and targeted to pleasure the palate, in a unique setting.
We offer various gastronomical concepts through dining, cooking and shopping. 
<br/>
<br/>
The architectural beauty of the building is one of the main ingredients of the “Origen experience”. <br/>
<br/>
Innovation and originality are the hallmarks that create our distinguished services. Origen is dedicated to providing a hassle free environment where wonderful memories and celebrations can be made.<br/>


</div>
</div>
</div>
